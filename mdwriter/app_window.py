# from gi.repository import Adw, GLib
from gettext import gettext as _
from pathlib import Path
from gi.repository import Gdk, Gio, Gtk
from mdwriter.confManager import ConfManager
from mdwriter.base_app import BaseWindow, AppShortcut
from mdwriter.app_content import AppContent
from mdwriter.dnd_helper import add_file_drop_target


class AppWindow(BaseWindow):
    def __init__(self):
        super().__init__(
            app_name='MD Writer',
            icon_name='org.gabmus.mdwriter',
            shortcuts=[AppShortcut(
                'F10', lambda *args: self.menu_btn.popup()
            )]
        )
        self.confman = ConfManager()

        self.app_content = AppContent()
        self.headerbar = self.app_content.headerbar  # necessary?
        self.menu_btn = self.app_content.menu_btn  # necessary?
        self.append(self.app_content)

        self.confman.connect(
            'dark_mode_changed',
            lambda *args: self.set_dark_mode(self.confman.conf['dark_mode'])
        )
        self.set_dark_mode(self.confman.conf['dark_mode'])
        self.connect('close-request', self.on_destroy)

        add_file_drop_target(self, self.on_dnd_drop)

    def on_dnd_drop(self, _dnd_target: Gtk.DropTarget, file: Gio.File, *_):
        p = Path(file.get_path())
        if not p.exists() or p.is_dir():
            return
        self.app_content.open_file(p)

    def present(self):
        super().present()
        self.set_default_size(
            self.confman.conf['windowsize']['width'],
            self.confman.conf['windowsize']['height']
        )

    def on_destroy(self, *__):
        unsaved = False
        for page in self.app_content.tabview.get_pages():
            if page.get_child().edited:
                unsaved = True
                break
        if unsaved:
            dialog = Gtk.MessageDialog(
                transient_for=self,
                modal=True,
                buttons=Gtk.ButtonsType.NONE,
                message_type=Gtk.MessageType.QUESTION,
                text=_('There Are Unsaved Changes'),
                secondary_text=_(
                    'Any unsaved change will be irrecoverably lost'
                )
            )
            dialog.add_button(
                _('Cancel'),
                Gtk.ResponseType.CANCEL
            )
            dialog.add_button(
                _('Discard and Quit'),
                Gtk.ResponseType.REJECT
            ).get_style_context().add_class('destructive-action')

            def on_response(_dialog, res):
                _dialog.close()
                if res == Gtk.ResponseType.REJECT:
                    for page in self.app_content.tabview.get_pages():
                        page.get_child().edited = False
                    self.close()
                elif res == Gtk.ResponseType.CANCEL:
                    pass

            dialog.connect('response', on_response)

            dialog.present()
            return Gdk.EVENT_STOP
        else:
            self.on_continue_destroy()

    def on_continue_destroy(self, *_):
        self.confman.conf['windowsize'] = {
            'width': self.get_width(),
            'height': self.get_height()
        }
        self.confman.trim_recents()
        self.confman.save_conf()

    def close_tab(self):
        self.app_content.close_tab()

    def save(self):
        self.app_content.save()

    def saveas(self):
        self.app_content.saveas()

    def open_document(self):
        self.app_content.on_open_clicked()

    def new_document(self):
        self.app_content.on_new_clicked()
