from datetime import datetime
from gettext import gettext as _
from pathlib import Path
from typing import Callable, Optional
from gi.repository import Adw, GLib, Gio, Gtk, GtkSource, GObject, WebKit2
from mdwriter.action_helper import new_action, new_action_group
from mdwriter.confManager import ConfManager
from mdwriter.dnd_helper import add_file_drop_target
from mdwriter.file_chooser import create_file_chooser
from mdwriter.recent_file_store import RecentFile, RecentFileStore
from mdwriter.snippet_store import Snippet
from mdwriter.snippets_view import SnippetsView
from mdwriter.md_style import build_css
from markdown import markdown


@Gtk.Template(resource_path='/org/gabmus/mdwriter/ui/editor_view.ui')
class EditorView(Gtk.Box):
    __gtype_name__ = 'EditorView'
    __gsignals__ = {
        'changed': (
            GObject.SignalFlags.RUN_LAST,
            None,
            (str,)
        )
    }
    sourceview: GtkSource.View = Gtk.Template.Child()
    sourcebuf: GtkSource.Buffer = Gtk.Template.Child()
    loading_bar: Gtk.ProgressBar = Gtk.Template.Child()
    loading_bar_revealer: Gtk.Revealer = Gtk.Template.Child()
    snippets_flap: Adw.Flap = Gtk.Template.Child()
    snippets_toggle_btn: Gtk.ToggleButton = Gtk.Template.Child()
    snippets_view: SnippetsView = Gtk.Template.Child()
    webview: WebKit2.WebView = Gtk.Template.Child()
    webview_revealer: Gtk.Revealer = Gtk.Template.Child()
    webview_inner_box: Gtk.Box = Gtk.Template.Child()
    splitview_box: Gtk.Box = Gtk.Template.Child()

    def __init__(
            self, recent_file_store: RecentFileStore,
            fpath: Optional[Path] = None
    ):
        super().__init__()
        self.needs_render = True
        self.fpath: Optional[Path] = fpath
        self.recent_file_store = recent_file_store
        self.__edited = False
        self.__paned_init = False
        assert(self.fpath is None or not self.fpath.is_dir())
        self.tab_page = None
        self.sourcebuf.set_language(
            GtkSource.LanguageManager.get_default().get_language('markdown')
        )
        self.confman = ConfManager()
        self.shortcut_controller = Gtk.ShortcutController()
        self.shortcut_controller.set_scope(Gtk.ShortcutScope.LOCAL)
        self.add_controller(self.shortcut_controller)
        new_action_group(
            self, 'editor', [
                new_action('bold', self.action_bold, '<Primary>b'),
                new_action('italic', self.action_italic, '<Primary>i'),
                new_action('strikethrough', self.action_strikethrough),
                new_action('code', self.action_code),
                new_action('heading1', lambda *_: self.action_heading(1)),
                new_action('heading2', lambda *_: self.action_heading(2)),
                new_action('heading3', lambda *_: self.action_heading(3)),
                new_action('heading4', lambda *_: self.action_heading(4)),
                new_action('heading5', lambda *_: self.action_heading(5)),
                new_action('heading6', lambda *_: self.action_heading(6)),
                new_action('image', self.action_image, '<Primary>m'),
                new_action('link', self.action_link, '<Primary>l'),
                new_action('quote', self.action_quote, '<Primary>k'),
                new_action('ulist', self.action_ulist),
                new_action('olist', self.action_olist),
                new_action('codeblock', self.action_codeblock),
                new_action('separator', self.action_separator),
                new_action('footnote', self.action_footnote),
            ], self.shortcut_controller
        )

        self.style_provider = None
        self.confman.connect(
            'editor_theme_changed', lambda *args: self.set_theme()
        )
        self.confman.connect(
            'editor_show_line_number_changed',
            lambda *args: self.set_line_number()
        )
        self.confman.connect(
            'editor_highlight_current_line_changed',
            lambda *args: self.set_highlight_current_line()
        )
        self.confman.connect(
            'editor_grid_pattern_changed',
            lambda *args: self.set_background_pattern()
        )
        self.confman.connect(
            'editor_font_changed',
            lambda *args: self.set_font()
        )
        self.set_theme()
        self.set_background_pattern()
        self.set_line_number()
        self.set_highlight_current_line()
        self.set_font()
        self.load_file()

        add_file_drop_target(self.sourceview, self.on_dnd_drop)
        self.set_preview_mode(self.confman.conf['preview_mode'])

        self.webview_settings = WebKit2.Settings(
            enable_javascript=False,
            enable_smooth_scrolling=True,
            enable_page_cache=False,
            enable_frame_flattening=False,
            enable_accelerated_2d_canvas=False,
            enable_developer_extras=True,
            allow_file_access_from_file_urls=False,
        )
        self.webview.set_settings(self.webview_settings)
        self.render_preview()

        self.connect('notify::edited', lambda *_: self.emit('changed', ''))

    def get_text(self) -> str:
        return self.sourcebuf.get_text(
            self.sourcebuf.get_start_iter(),
            self.sourcebuf.get_end_iter(), True
        )

    def render_preview(self):
        if self.needs_render:
            self.needs_render = False
            if self.confman.conf['preview_mode'] == 'disabled':
                GLib.timeout_add(5000, self.render_preview)
                return
            content = markdown(self.get_text(), extensions=[
                'tables', 'sane_lists', 'markdown_del_ins'
            ])
            html = '''
                <!DOCTYPE html><html lang="en">
                <head><meta charset="UTF-8"><style>{0}</style></head>
                <body>{1}</body></html>
            '''.format(build_css(self.sourcebuf), content)
            self.webview.load_html(html, None)
        GLib.timeout_add(1000, self.render_preview)

    @Gtk.Template.Callback()
    def on_webview_decide_policy(self, _, decision, decision_type):
        if (
                decision_type in (
                    WebKit2.PolicyDecisionType.NAVIGATION_ACTION,
                    WebKit2.PolicyDecisionType.NEW_WINDOW_ACTION
                ) and
                decision.get_navigation_action().get_mouse_button() != 0
        ):
            decision.ignore()
            target = decision.get_navigation_action().get_request().get_uri()
            # TODO show a dialog asking to open or not the link
            Gio.AppInfo.launch_default_for_uri(target)
            return True
        return False

    def on_dnd_drop(self, _dnd_target: Gtk.DropTarget, file: Gio.File, *_):
        root = self.get_root()
        if root:
            root.on_dnd_drop(_dnd_target, file)

    def load_file(self):
        if self.fpath is None:
            return
        assert(not self.fpath.is_dir())
        with open(self.fpath, 'r') as fd:
            self.sourcebuf.set_text(fd.read())
        self.set_tab_title()

    def set_tab_page(self, tab_page: Adw.TabPage):
        self.tab_page = tab_page
        self.set_tab_title()

    def gen_tab_title(self) -> str:
        if self.fpath is not None:
            return self.fpath.name
        else:
            return _('Untitled')

    def set_tab_title(self):
        if self.tab_page is None:
            return
        self.tab_page.set_title(self.gen_tab_title())

    def set_theme(self):
        self.sourcebuf.set_style_scheme(
            GtkSource.StyleSchemeManager.get_default().get_scheme(
                self.confman.conf['editor_theme']
            ) or GtkSource.StyleSchemeManager.get_default().get_scheme(
                'Adwaita'
            )
        )
        self.needs_render = True

    def set_background_pattern(self):
        self.sourceview.set_background_pattern(
            GtkSource.BackgroundPatternType.GRID
            if self.confman.conf['editor_grid_pattern']
            else GtkSource.BackgroundPatternType.NONE
        )

    def set_line_number(self):
        self.sourceview.set_show_line_numbers(
            self.confman.conf['editor_show_line_number']
        )

    def set_highlight_current_line(self):
        self.sourceview.set_highlight_current_line(
            self.confman.conf['editor_highlight_current_line']
        )

    def set_font(self):
        # unset style
        if self.style_provider is not None:
            self.sourceview.get_style_context().remove_provider(
                self.style_provider
            )
        if not self.confman.conf['editor_font_enabled']:
            return
        font = self.confman.conf['editor_font']
        family = ' '.join(font.split(' ')[:-1])
        size = str(max(int(font.split(' ')[-1]), 3))
        self.style_provider = Gtk.CssProvider()
        self.style_provider.load_from_data(
            f'''.sourceview {{
                    font-family: {family}, monospace;
                    font-size: {size}pt;
            }}'''.encode()
        )
        self.sourceview.get_style_context().add_provider(
            self.style_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )

    @GObject.Property(type=bool, default=False)
    def edited(self) -> bool:
        return self.__edited

    @edited.setter
    def edited(self, state: bool):
        self.__edited = state

    def set_edited(self, state: bool):
        if self.edited == state or self.tab_page is None:
            return
        self.edited = state
        self.tab_page.set_icon(
            Gio.ThemedIcon(name='unsaved-symbolic') if state else None
        )

    @Gtk.Template.Callback()
    def on_sourcebuf_changed(self, *_):
        self.set_edited(True)
        self.needs_render = True

    def __get_insert_iter(self) -> Gtk.TextIter:
        return self.sourcebuf.get_iter_at_mark(
            self.sourcebuf.get_mark('insert')
        )

    def __get_select_iter(self) -> Gtk.TextIter:
        self.sourcebuf.get_iter_at_mark(
            self.sourcebuf.get_mark('selection_bound')
        )

    def __get_chars_after_iter(self, iter: Gtk.TextIter, n: int = 1) -> str:
        return self.sourcebuf.get_text(
            iter,
            self.sourcebuf.get_iter_at_offset(
                iter.get_offset() + n
            ),
            True
        )

    def __del_chars_after_iter(self, iter: Gtk.TextIter, n: int = 1) -> str:
        return self.sourcebuf.delete(
            iter,
            self.sourcebuf.get_iter_at_offset(
                iter.get_offset() + n
            )
        )

    def __surround(self, chars: str):
        selection_bounds = self.sourcebuf.get_selection_bounds()
        if not selection_bounds:
            self.sourcebuf.insert(self.__get_insert_iter(), chars*2)
            self.sourcebuf.place_cursor(self.sourcebuf.get_iter_at_offset(
                self.__get_insert_iter().get_offset() - len(chars)
            ))
        else:
            end_offset = selection_bounds[1].get_offset()
            self.sourcebuf.insert(selection_bounds[0], chars)
            self.sourcebuf.insert(self.sourcebuf.get_iter_at_offset(
                end_offset + len(chars)
            ), chars)
        self.sourceview.grab_focus()

    def action_bold(self, *_):
        self.__surround('**')

    def action_italic(self, *_):
        self.__surround('_')

    def action_strikethrough(self, *_):
        self.__surround('~~')

    def action_code(self, *_):
        self.__surround('`')

    def action_separator(self, *_):
        it = self.__get_insert_iter()
        cur_iter = self.__get_insert_iter()
        it.forward_to_line_end()
        if cur_iter.get_line() != it.get_line():
            it = cur_iter
        self.sourcebuf.insert(it, '\n\n---\n\n')
        self.sourceview.grab_focus()

    def action_heading(self, level: int = 1):
        insert_iter = self.__get_insert_iter()
        line_start = self.sourcebuf.get_iter_at_line_offset(
            insert_iter.get_line(), 0
        )[1]
        nxtc = self.__get_chars_after_iter(line_start)
        if nxtc == '#':
            while True:
                self.__del_chars_after_iter(line_start)
                nxtc = self.__get_chars_after_iter(line_start)
                if nxtc == '\n' or nxtc not in ('#', ' '):
                    break
        if level <= 0:
            return
        level = min(level, 6)
        self.sourcebuf.insert(line_start, ('#' * level) + ' ')
        self.sourceview.grab_focus()

    def __link(self, image: bool = False):
        selection_bounds = self.sourcebuf.get_selection_bounds()
        text_plch = _('text...')
        link_plch = _('link...')
        if not selection_bounds:
            self.sourcebuf.insert(
                self.__get_insert_iter(),
                ('!' if image else '') + f'[{text_plch}]({link_plch})'
            )
        else:
            end_offset = selection_bounds[1].get_offset()
            start_offset = selection_bounds[0].get_offset()
            selected_text = self.sourcebuf.get_text(*selection_bounds, False)
            if 'https://' in selected_text or 'http://' in selected_text:
                self.sourcebuf.insert(
                    selection_bounds[0],
                    ('!' if image else '') + f'[{text_plch}]('
                )
                self.sourcebuf.insert(
                    self.sourcebuf.get_iter_at_offset(
                        end_offset +
                        (1 if image else 0) +
                        len(f'[{text_plch}](')
                    ),
                    ')'
                )
                self.sourcebuf.place_cursor(
                    self.sourcebuf.get_iter_at_offset(
                        start_offset + (1 if image else 0) +
                        len(f'[{text_plch}')
                    )
                )
            else:
                self.sourcebuf.insert(
                    selection_bounds[0],
                    ('!' if image else '') + '['
                )
                self.sourcebuf.insert(
                    self.sourcebuf.get_iter_at_offset(
                        end_offset + (1 if image else 0) + 1
                    ),
                    f']({link_plch})'
                )
                self.sourcebuf.place_cursor(
                    self.sourcebuf.get_iter_at_offset(
                        end_offset + (1 if image else 0) + 1 +
                        len(f']({link_plch}')
                    )
                )
        self.sourceview.grab_focus()

    def action_image(self, *_):
        self.__link(True)

    def action_link(self, *_):
        self.__link(False)

    def __prefix_rows_in_selection(self, chars: str):
        start_iter, end_iter = (None, None)
        if self.sourcebuf.get_has_selection():
            start_iter, end_iter = self.sourcebuf.get_selection_bounds()
        else:
            start_iter = end_iter = self.__get_insert_iter()
        end_line = end_iter.get_line()
        is_add = None
        while True:
            start_line = start_iter.get_line()
            line_start = self.sourcebuf.get_iter_at_line_offset(
                start_iter.get_line(), 0
            )[1]

            nxtc = self.__get_chars_after_iter(line_start, len(chars))
            if is_add is None:
                is_add = nxtc != chars
            if nxtc == chars:
                if not is_add:
                    self.__del_chars_after_iter(line_start, len(chars))
            else:
                if is_add:
                    self.sourcebuf.insert(line_start, chars)

            if start_line == end_line:
                break
            start_iter = self.sourcebuf.get_iter_at_line_index(
                start_line + 1, 0
            )[1]
        self.sourceview.grab_focus()

    def action_quote(self, *_):
        self.__prefix_rows_in_selection('> ')

    def action_ulist(self, *_):
        self.__prefix_rows_in_selection('- ')

    def action_olist(self, *_):
        start_iter, end_iter = (None, None)
        if self.sourcebuf.get_has_selection():
            start_iter, end_iter = self.sourcebuf.get_selection_bounds()
        else:
            start_iter = end_iter = self.__get_insert_iter()
        end_line = end_iter.get_line()
        is_add = None
        line = 1
        chars = f'{line}. '
        while True:
            start_line = start_iter.get_line()
            line_start = self.sourcebuf.get_iter_at_line_offset(
                start_iter.get_line(), 0
            )[1]

            nxtc = self.__get_chars_after_iter(line_start, len(chars))
            if is_add is None:
                is_add = nxtc != chars
            if nxtc == chars:
                if not is_add:
                    self.__del_chars_after_iter(line_start, len(chars))
            else:
                if is_add:
                    self.sourcebuf.insert(line_start, chars)

            if start_line == end_line:
                break
            start_iter = self.sourcebuf.get_iter_at_line_index(
                start_line + 1, 0
            )[1]
            line += 1
            chars = f'{line}. '
        self.sourceview.grab_focus()

    def action_codeblock(self, *__):
        start_iter, end_iter = (None, None)
        if self.sourcebuf.get_has_selection():
            start_iter, end_iter = self.sourcebuf.get_selection_bounds()
        else:
            ins_iter = self.__get_insert_iter()
            init_ins_iter = self.__get_insert_iter()
            ins_iter.forward_to_line_end()
            self.sourcebuf.insert(
                ins_iter,
                (
                    ''
                    if ins_iter.get_offset() == init_ins_iter.get_offset()
                    and self.sourcebuf.get_iter_at_line_offset(
                        init_ins_iter.get_line(), 0
                    )[1].get_offset() == init_ins_iter.get_offset()
                    else '\n\n'
                ) + '```\n' + _('code...') + '\n```\n\n'
            )
            self.sourceview.grab_focus()
            return
        start_line = start_iter.get_line()
        end_line = end_iter.get_line()

        it = self.sourcebuf.get_iter_at_line_offset(end_line, 0)[1]
        it.forward_to_line_end()
        self.sourcebuf.insert(it, '\n```\n')

        it = self.sourcebuf.get_iter_at_line_offset(start_line, 0)[1]
        self.sourcebuf.insert(it, '\n```\n')

        self.sourceview.grab_focus()

    def action_footnote(self, *__):
        index = 1
        txt = self.get_text()
        while f'[^{index}]' in txt:
            index += 1
        self.sourcebuf.insert(self.__get_insert_iter(), f'[^{index}]')
        it = self.__get_insert_iter()
        it.forward_to_line_end()
        self.sourcebuf.insert(
            it, f'\n\n[^{index}]: ' + _('footnote...') + '\n\n'
        )
        self.sourceview.grab_focus()

    def __write_file(self, callback: Optional[Callable] = None):
        # TODO: do this async
        assert(self.fpath is not None and not self.fpath.is_dir())
        with open(self.fpath, 'w') as fd:
            fd.write(self.get_text())
        self.recent_file_store.add_rf(
            RecentFile(self.fpath, datetime.now())
        )
        self.set_edited(False)
        if callback is not None:
            callback(True)

    def action_saveas(self, *__, callback: Optional[Callable] = None):
        self.loading_bar_revealer.set_reveal_child(True)
        self.__fc_filter = Gtk.FileFilter()
        self.__fc_filter.add_suffix('md')

        def on_accept(dialog: Gtk.FileChooserDialog):
            self.fpath = Path(dialog.get_file().get_path())
            self.set_tab_title()
            self.__write_file()
            self.loading_bar_revealer.set_reveal_child(False)
            if callback is not None:
                callback(True)
            self.emit('changed', '')

        def on_cancel(_):
            self.loading_bar_revealer.set_reveal_child(False)
            if callback is not None:
                callback(False)

        self.__fc = create_file_chooser(
            title=_('Save As...'),
            action=Gtk.FileChooserAction.SAVE,
            parent=self.get_root(),
            on_accept=on_accept,
            on_cancel=on_cancel,
            filter=self.__fc_filter
        )

        self.__fc.present()

    def action_save(self, *_, callback: Optional[Callable] = None):
        self.loading_bar_revealer.set_reveal_child(True)
        if self.fpath is None:
            return self.action_saveas(callback=callback)
        else:
            assert(not self.fpath.is_dir())
            if self.fpath.is_file() or self.fpath.parent.is_dir():
                self.__write_file(callback=callback)
            else:
                self.fpath.parent.mkdir(parents=True)
                self.__write_file(callback=callback)

        GLib.timeout_add(
            350, lambda *_: self.loading_bar_revealer.set_reveal_child(False)
        )

    def on_close_tab(self, tabview: Adw.TabView, page: Adw.TabPage):
        if self.edited:
            # TODO: are you sure?
            dialog = Gtk.MessageDialog(
                transient_for=self.get_root(),
                modal=True,
                buttons=Gtk.ButtonsType.NONE,
                message_type=Gtk.MessageType.QUESTION,
                text=_('Save Changes?'),
                secondary_text=_(
                    'Document {0} contains unsaved changes.'
                ).format(self.fpath.name if self.fpath else _('Untitled'))
            )

            dialog.add_button(
                _('Cancel'), Gtk.ResponseType.CANCEL
            )
            dialog.add_button(
                _('Discard'), Gtk.ResponseType.REJECT
            ).get_style_context().add_class('destructive-action')
            dialog.add_button(
                _('Save'), Gtk.ResponseType.ACCEPT
            ).get_style_context().add_class('suggested-action')

            def on_response(_dialog, res):
                _dialog.close()
                if res == Gtk.ResponseType.REJECT:
                    tabview.close_page_finish(self.tab_page, True)
                elif res == Gtk.ResponseType.ACCEPT:
                    self.action_save(
                        callback=lambda ok: tabview.close_page_finish(
                            page, ok
                        )
                    )
                elif res == Gtk.ResponseType.CANCEL:
                    tabview.close_page_finish(page, False)

            dialog.connect('response', on_response)
            dialog.present()
            return
        else:
            tabview.close_page_finish(page, True)

    @Gtk.Template.Callback()
    def on_snippets_toggle_btn_toggled(self, *_):
        self.snippets_flap.set_reveal_flap(
            self.snippets_toggle_btn.get_active()
        )

    @Gtk.Template.Callback()
    def on_reveal_flap(self, *_):
        self.snippets_toggle_btn.set_active(
            self.snippets_flap.get_reveal_flap()
        )

    @Gtk.Template.Callback()
    def on_snippet_selected(self, _, snippet: Snippet):
        it = self.__get_insert_iter()
        cur_iter = self.__get_insert_iter()
        it.forward_to_line_end()
        if it.get_line() != cur_iter.get_line():
            it = cur_iter
        self.sourcebuf.insert(it, '\n\n' + snippet.text + '\n\n')
        self.snippets_flap.set_reveal_flap(False)

    def set_preview_mode(self, n_mode: str):
        if n_mode == 'horizontal':
            self.webview_revealer.set_visible(True)
            self.splitview_box.set_orientation(Gtk.Orientation.HORIZONTAL)
            self.webview_inner_box.set_orientation(Gtk.Orientation.HORIZONTAL)
            self.webview_revealer.set_reveal_child(True)
        elif n_mode == 'disabled':
            self.webview_revealer.set_visible(False)
            self.webview_revealer.set_reveal_child(False)
        else:  # vertical
            self.webview_revealer.set_visible(True)
            self.splitview_box.set_orientation(Gtk.Orientation.VERTICAL)
            self.webview_inner_box.set_orientation(Gtk.Orientation.VERTICAL)
            self.webview_revealer.set_reveal_child(True)
