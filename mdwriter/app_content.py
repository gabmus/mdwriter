from gettext import gettext as _
from pathlib import Path
from typing import Optional
from gi.repository import Gdk, Gtk, Adw
from mdwriter.editor_view import EditorView
from mdwriter.file_chooser import create_file_chooser
from mdwriter.recent_file_row import RecentFileRow
from mdwriter.recent_file_store import RecentFile, RecentFileStore


@Gtk.Template(resource_path='/org/gabmus/mdwriter/ui/app_content.ui')
class AppContent(Gtk.Box):
    __gtype_name__ = 'AppContent'
    headerbar: Gtk.HeaderBar = Gtk.Template.Child()
    menu_btn: Gtk.MenuButton = Gtk.Template.Child()
    tabbar: Adw.TabBar = Gtk.Template.Child()
    tabview: Adw.TabView = Gtk.Template.Child()
    recent_files_listbox: Gtk.ListBox = Gtk.Template.Child()
    recent_files_popover: Gtk.Popover = Gtk.Template.Child()
    unsaved_icon: Gtk.Image = Gtk.Template.Child()
    title_label: Gtk.Label = Gtk.Template.Child()
    stack: Gtk.Stack = Gtk.Template.Child()
    empty_state: Adw.StatusPage = Gtk.Template.Child()
    main_content: Gtk.Box = Gtk.Template.Child()

    def __init__(self):
        super().__init__()
        self.recent_file_store = RecentFileStore()
        self.recent_files_listbox.bind_model(
            self.recent_file_store,
            self.__create_recent_file_row,
            None
        )

    @Gtk.Template.Callback()
    def on_tabview_close_page(self, tabview: Adw.TabView, page: Adw.TabPage):
        page.get_child().on_close_tab(tabview, page)
        return Gdk.EVENT_STOP

    @Gtk.Template.Callback()
    def on_num_pages_changed(self, *_):
        self.stack.set_visible_child(
            self.empty_state
            if self.tabview.get_n_pages() <= 0
            else self.main_content
        )

    @Gtk.Template.Callback()
    def on_recent_files_listbox_row_activated(self, _, row: RecentFileRow):
        self.__create_tab(row.rf.path)
        self.recent_files_popover.popdown()

    @Gtk.Template.Callback()
    def on_page_selected(self, *_):
        page = self.tabview.get_selected_page()
        if page:
            self.title_label.set_text(page.get_child().gen_tab_title())
            self.unsaved_icon.set_visible(page.get_icon() is not None)
        else:
            self.title_label.set_text('MD Writer')
            self.unsaved_icon.set_visible(False)

    def on_document_changed(self, editor: EditorView, _):
        self.unsaved_icon.set_visible(editor.edited)
        self.title_label.set_text(editor.gen_tab_title())

    def __create_recent_file_row(self, rf: RecentFile, *_) -> Gtk.ListBoxRow:
        row = RecentFileRow(rf)
        return row

    def __create_tab(self, fpath: Optional[Path] = None):
        if fpath is not None:
            for page in self.tabview.get_pages():
                cur_page_fpath = page.get_child().fpath
                if cur_page_fpath == fpath:
                    self.tabview.set_selected_page(page)
                    return
        n_view = EditorView(
            recent_file_store=self.recent_file_store,
            fpath=fpath
        )
        n_view.connect('changed', self.on_document_changed)
        tab_page = self.tabview.append(n_view)
        n_view.set_tab_page(tab_page)
        self.tabview.set_selected_page(tab_page)
        n_view.sourceview.grab_focus()

    def open_file(self, fpath: Optional[Path] = None):
        self.__create_tab(fpath)

    def close_tab(self):
        page = self.tabview.get_selected_page()
        if page:
            self.tabview.close_page(page)

    def save(self):
        page = self.tabview.get_selected_page()
        if page:
            page.get_child().action_save()

    def saveas(self):
        page = self.tabview.get_selected_page()
        if page:
            page.get_child().action_saveas()

    @Gtk.Template.Callback()
    def on_open_clicked(self, *__):
        self.__fc_filter = Gtk.FileFilter()
        # vvv this causes the chooser to ignore the suffix
        # self.__fc_filter.add_mime_type('text/plain')
        self.__fc_filter.add_suffix('md')

        def on_accept(dialog: Gtk.FileChooserDialog):
            fpath = Path(dialog.get_file().get_path())
            self.__create_tab(fpath)

        self.__fc = create_file_chooser(
            title=_('Open a Markdown Document'),
            action=Gtk.FileChooserAction.OPEN,
            parent=self.get_root(),
            on_accept=on_accept,
            filter=self.__fc_filter
        )
        self.__fc.present()

    @Gtk.Template.Callback()
    def on_new_clicked(self, *_):
        self.__create_tab(None)
