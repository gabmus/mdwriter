from typing import Callable
from gi.repository import GObject, Gio, Gdk, Gtk


def add_file_drop_target(target: Gtk.Widget, func: Callable):
    target.__dnd_target = Gtk.DropTarget.new(
        GObject.GType(Gio.File), Gdk.DragAction.COPY
    )
    target.add_controller(target.__dnd_target)
    target.__dnd_target.connect('drop', func)
