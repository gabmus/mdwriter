from gi.repository import GObject, Gtk, Gio
from mdwriter.confManager import ConfManager


class Snippet(GObject.Object):
    __name: str = ''
    __text: str = ''

    def __init__(self, name: str, text: str):
        super().__init__()
        self.name = name
        self.text = text

    @GObject.Property(type=str)
    def name(self) -> str:
        return self.__name

    @name.setter
    def name(self, n_name: str):
        self.__name = n_name

    @GObject.Property(type=str)
    def text(self) -> str:
        return self.__text

    @text.setter
    def text(self, n_text: str):
        self.__text = n_text

    @classmethod
    def from_dict(cls, d: dict):
        assert('name' in d.keys() and 'text' in d.keys())
        return cls(name=d['name'], text=d['text'])

    def to_dict(self) -> dict:
        return {
            'name': self.name,
            'text': self.text
        }


class SnippetStore(Gtk.SortListModel):
    instance = None

    def __init__(self):
        self.confman = ConfManager()

        self.sorter = Gtk.CustomSorter()
        self.sorter.set_sort_func(self.__sort_func)
        self.list_store = Gio.ListStore(item_type=Snippet)
        super().__init__(model=self.list_store, sorter=self.sorter)
        self.populate()

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = cls()
        return cls.instance

    def __sort_func(self, s1: Snippet, s2: Snippet, *_) -> int:
        return -1 if s1.name < s2.name else 1

    def invalidate_sort(self):
        self.sorter.set_sort_func(self.__sort_func)

    def empty(self):
        self.list_store.remove_all()

    def populate(self):
        self.empty()
        for sd in self.confman.conf['snippets']:
            snippet = Snippet.from_dict(sd)
            self.list_store.append(snippet)

    def dump_to_conf(self):
        self.confman.conf['snippets'] = [s.to_dict() for s in self.list_store]
        self.confman.save_conf()

    def add_snippet(self, snippet: Snippet):
        for exs in self.list_store:
            if exs.name == snippet.name:
                exs.text = snippet.text
                self.dump_to_conf()
                return
        self.list_store.append(snippet)
        self.dump_to_conf()

    def remove_snippet(self, name: str):
        for i, snip in enumerate(self.list_store):
            if snip.name == name:
                self.list_store.remove(i)
                self.dump_to_conf()
                return
