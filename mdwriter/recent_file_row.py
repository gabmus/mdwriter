from gi.repository import Gtk
from mdwriter.recent_file_store import RecentFile


@Gtk.Template(resource_path='/org/gabmus/mdwriter/ui/recent_file_row.ui')
class RecentFileRow(Gtk.ListBoxRow):
    __gtype_name__ = 'RecentFileRow'
    name_label = Gtk.Template.Child()
    path_label = Gtk.Template.Child()

    def __init__(self, rf: RecentFile):
        super().__init__()
        self.rf = rf
        self.name_label.set_text(self.rf.path.name)
        self.path_label.set_text(str(self.rf.path.parent))
