from pathlib import Path
from os.path import isfile
from os import environ as Env
import json
from gi.repository import GObject
from mdwriter.singleton import Singleton


class ConfManagerSignaler(GObject.Object):
    __gsignals__ = {
        'dark_mode_changed': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        ),
        'editor_theme_changed': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        ),
        'editor_grid_pattern_changed': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        ),
        'editor_font_changed': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        ),
        'editor_show_line_number_changed': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        ),
        'editor_highlight_current_line_changed': (
            GObject.SignalFlags.RUN_FIRST,
            None,
            (str,)
        ),
    }


class ConfManager(metaclass=Singleton):

    BASE_SCHEMA = {
        'dark_mode': False,
        'windowsize': {
            'width': 350,
            'height': 650
        },
        'editor_theme': 'Adwaita',
        'editor_grid_pattern': False,
        'editor_show_line_number': False,
        'editor_highlight_current_line': False,
        'editor_font_enabled': False,
        'editor_font': '',
        # recent file:
        # duplication of path for easier access
        # path: {
        #     'path': Path -> str,
        #     'last_modified': datetime -> timestamp (float)
        # }
        'recent_files': {},
        'max_recents': 10,
        # snippet:
        # {'name': str, 'text': str}
        'snippets': [],
        'preview_mode': 'vertical',  # vertical, horizontal, disabled
    }

    def __init__(self):
        self.signaler = ConfManagerSignaler()
        self.emit = self.signaler.emit
        self.connect = self.signaler.connect

        self.is_flatpak = isfile('/.flatpak-info')

        self.conf_dir = Path(
            Env.get('XDG_CONFIG_HOME') or f'{Env.get("HOME")}/.config'
        )
        self.cache_home = Path(
            Env.get('XDG_CACHE_HOME') or f'{Env.get("HOME")}/.cache'
        )
        self.cache_path = self.cache_home.joinpath('org.gabmus.mdwriter')
        for p in [
                self.conf_dir,
                self.cache_path,
        ]:
            if not p.is_dir():
                p.mkdir(parents=True)
        self.path = self.conf_dir.joinpath('org.gabmus.mdwriter.json')

        if self.path.is_file():
            try:
                with open(self.path) as fd:
                    self.conf = json.loads(fd.read())
                # verify that the file has all of the schema keys
                for k in ConfManager.BASE_SCHEMA:
                    if k not in self.conf.keys():
                        if isinstance(
                                ConfManager.BASE_SCHEMA[k], (list, dict)
                        ):
                            self.conf[k] = ConfManager.BASE_SCHEMA[k].copy()
                        else:
                            self.conf[k] = ConfManager.BASE_SCHEMA[k]
            except Exception:
                self.conf = ConfManager.BASE_SCHEMA.copy()
                self.save_conf(force_overwrite=True)
        else:
            self.conf = ConfManager.BASE_SCHEMA.copy()
            self.save_conf()

    def trim_recents(self):
        rm_num = len(self.conf['recent_files']) - self.conf['max_recents']
        if rm_num > 0:
            to_rm = sorted(
                self.conf['recent_files'],
                key=lambda path:
                    self.conf['recent_files'][path]['last_modified']
            )[:rm_num]
            for rfp in to_rm:
                self.conf['recent_files'].pop(rfp)
            self.save_conf()

    def save_conf(self, *args, force_overwrite=False):
        if self.path.is_file() and not force_overwrite:
            with open(self.path, 'r') as fd:
                if json.loads(fd.read()) == self.conf:
                    return
        with open(self.path, 'w') as fd:
            fd.write(json.dumps(self.conf))
